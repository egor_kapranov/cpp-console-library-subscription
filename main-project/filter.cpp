#include "filter.h"
#include <cstring>
#include <iostream>


internet_config** filter(internet_config* array[], int size, bool (*check)(internet_config* element), int& result_size)
{
	internet_config** result = new internet_config * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_internet_config_by_programm(internet_config* element)
{
	return strcmp(element->start.chaz, " ") >= 8;
}

bool check_internet_config_by_date_chaz(internet_config* element)
{
	return element->programm == "D:\Skype";
}
