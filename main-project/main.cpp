#include <iostream>
#include <iomanip>

using namespace std;

#include "internet_config.h"
#include "file_reader.h"
#include "constants.h"
#include"filter.h"
void output(internet_config* subscriptions) {
/********** ����� �������� **********/
            cout << "�����........: ";
            // ����� �������
            cout << subscriptions->otprav.obait << " ";
            // ����� ������ ����� �����
            cout << subscriptions->polych.obait << ". ";
            cout << '\n';
            /********** ����� ����� **********/
            cout << "���� �����...........: ";
            // ����� ��������
            cout << '"' << subscriptions->programm << '"';
            cout << '\n';
            /********** ����� ���� ������ **********/
            // ����� ����
            cout << "���� ������.....: ";
            cout << setw(4) << setfill('0') << subscriptions->start.sek << '-';
            // ����� ������
            cout << setw(2) << setfill('0') << subscriptions->start.min << '-';
            // ����� �����
            cout << setw(2) << setfill('0') << subscriptions->start.chaz;
            cout << '\n';
            /********** ����� ���� �������� **********/
            // ����� ����
            cout << "���� �����...: ";
            cout << setw(4) << setfill('0') << subscriptions->finish.sek << '-';
            // ����� ������
            cout << setw(2) << setfill('0') << subscriptions->finish.min << '-';
            // ����� �����
            cout << setw(2) << setfill('0') << subscriptions->finish.chaz;
            cout << '\n';
            cout << '\n';
}


int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� 5. ������������ ���������\n";
    cout << "�����: �������� ����\n\n";
    internet_config* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        cout << "***** ��������� � �������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            output(subscriptions[i]);
        }
        bool (*check_function)(internet_config*) = NULL;
        cout << "\n     :\n";
        cout << "1)       \n";
        cout << "2)        2015- \n";
        cout << "3)   ,      \n";
        cout << "\n   : ";
        int item;
        cin >> item;
        cout << '\n';
        switch (item) {
        case 1:
            check_function = check_internet_config_by_programm;
            cout << "***   ***\n\n";
            break;
        case 2:
            check_function = check_internet_config_by_date_chaz;
            cout << "*** ***\n\n";
            break;
        default:
            throw"  ";
        }
        if (check_function)
        {
            int new_size;
            internet_config** filtered = filter(subscriptions, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    cout << "Group: 11\n";
    return 0;
}
