#ifndef BOOK_SUBSCRIPTION_H
#define BOOK_SUBSCRIPTION_H
#include <string>
#include "constants.h"
 
using namespace std;

struct date
{
    int sek;
    int min;
    int chaz;
};

struct bait
{
    int obait;
};

struct internet_config
{
    string programm;
    date start;
    date finish;
    bait polych;
    bait otprav;
};

#endif