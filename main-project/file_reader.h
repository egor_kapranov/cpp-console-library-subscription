#ifndef FILE_READER_H
#define FILE_READER_H

#include "internet_config.h"

void read(const char* file_name, internet_config* array[], int& size);

#endif